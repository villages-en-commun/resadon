require 'test_helper'

class EmpruntablesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @empruntable = empruntables(:one)
  end

  test "should get index" do
    get empruntables_url
    assert_response :success
  end

  test "should get new" do
    get new_empruntable_url
    assert_response :success
  end

  test "should create empruntable" do
    assert_difference('Empruntable.count') do
      post empruntables_url, params: { empruntable: { description_courte: @empruntable.description_courte, description_longue: @empruntable.description_longue, nom: @empruntable.nom } }
    end

    assert_redirected_to empruntable_url(Empruntable.last)
  end

  test "should show empruntable" do
    get empruntable_url(@empruntable)
    assert_response :success
  end

  test "should get edit" do
    get edit_empruntable_url(@empruntable)
    assert_response :success
  end

  test "should update empruntable" do
    patch empruntable_url(@empruntable), params: { empruntable: { description_courte: @empruntable.description_courte, description_longue: @empruntable.description_longue, nom: @empruntable.nom } }
    assert_redirected_to empruntable_url(@empruntable)
  end

  test "should destroy empruntable" do
    assert_difference('Empruntable.count', -1) do
      delete empruntable_url(@empruntable)
    end

    assert_redirected_to empruntables_url
  end
end
