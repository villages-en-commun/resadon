require "application_system_test_case"

class EmpruntablesTest < ApplicationSystemTestCase
  setup do
    @empruntable = empruntables(:one)
  end

  test "visiting the index" do
    visit empruntables_url
    assert_selector "h1", text: "Empruntables"
  end

  test "creating a Empruntable" do
    visit empruntables_url
    click_on "New Empruntable"

    fill_in "Description courte", with: @empruntable.description_courte
    fill_in "Description longue", with: @empruntable.description_longue
    fill_in "Nom", with: @empruntable.nom
    click_on "Create Empruntable"

    assert_text "Empruntable was successfully created"
    click_on "Back"
  end

  test "updating a Empruntable" do
    visit empruntables_url
    click_on "Edit", match: :first

    fill_in "Description courte", with: @empruntable.description_courte
    fill_in "Description longue", with: @empruntable.description_longue
    fill_in "Nom", with: @empruntable.nom
    click_on "Update Empruntable"

    assert_text "Empruntable was successfully updated"
    click_on "Back"
  end

  test "destroying a Empruntable" do
    visit empruntables_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Empruntable was successfully destroyed"
  end
end
