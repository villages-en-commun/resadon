class UserMailer < ApplicationMailer
  default from: 'contact@chateldon.fr'

  def confirmation
    @emprunt = params[:emprunt]
    mail(to: @emprunt.email, subject: '[Resadon] Confirmation de votre réservation')
  end
end
