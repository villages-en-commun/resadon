function app() {
  return {
    showDatepicker: false,
    datepickerValue: '',
    MONTH_NAMES: ['Janvier', 'Fevrier', 'Mars', 'Avril', 'Mai', 'Juin', 'Juillet', 'Août', 'Septembre', 'Octobre', 'Novembre', 'Decembre'],
    DAYS: ['Lun', 'Mar', 'Mer', 'Jeu', 'Ven', 'Sam', 'Dim'],
    firstDayOfWeek: 1,

    month: '',
    year: '',
    no_of_days: [],
    blankdays: [],

    initDate() {
      let today = new Date();
      this.month = today.getMonth();
      this.year = today.getFullYear();
      this.datepickerValue = "";
    },

    isToday(date) {
      const today = new Date();
      const d = new Date(this.year, this.month, date);

      return today.toDateString() === d.toDateString() ? true : false;
    },
    isAllowed(date) {
      const d = new Date(this.year, this.month, date)
      if (d < new Date()) {
        return false;
      }
      return (!gon.dates_non_disponibles.includes(d.toLocaleDateString('fr')));
    },

    getDateValue(date) {
      if (this.isAllowed(date) == true) {
        let selectedDate = new Date(this.year, this.month, date);
        this.datepickerValue = selectedDate.toLocaleDateString('fr');

        this.$refs.date.value = this.datepickerValue;

        console.log(this.$refs.date.value);

        this.showDatepicker = false;
      }
    },

    getNoOfDays() {
      let daysInMonth = new Date(this.year, this.month + 1, 0).getDate();

      // find where to start calendar day of week
      let dayOfWeek = new Date(this.year, this.month).getDay() - this.firstDayOfWeek;
      let blankdaysArray = [];
      for ( var i=1; i <= dayOfWeek; i++) {
        blankdaysArray.push(i);
      }

      let daysArray = [];
      for ( var i=1; i <= daysInMonth; i++) {
        daysArray.push(i);
      }

      this.blankdays = blankdaysArray;
      this.no_of_days = daysArray;
    }
  }
}

