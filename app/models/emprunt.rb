class Emprunt < ApplicationRecord
  belongs_to :empruntable
  validates :email, format: { with: /.+@.+\.\w+/ }, presence: true
  validates :date_emprunt, presence: true
  validates :date_restitution, presence: true
end
