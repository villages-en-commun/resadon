class Empruntable < ApplicationRecord
  has_one_attached :photo
  has_many :emprunts

  def to_param
    "#{id}-#{nom.parameterize}"
  end

  def non_disponible
    emprunts.flat_map{|emprunt| (emprunt.date_emprunt..emprunt.date_restitution).to_a.map{|d| d.strftime('%m/%d/%Y')}}
  end
end
