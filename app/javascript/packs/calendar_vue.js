import TurbolinksAdapter from 'vue-turbolinks'
import Vue from 'vue/dist/vue.esm'
import Datepicker from 'vuejs-datepicker'
import { fr } from 'vuejs-datepicker/dist/locale'

Vue.use(TurbolinksAdapter)

document.addEventListener('turbolinks:load', () => {
  const app = new Vue({
    el: '#calendar',
    data: () => {
      return {
        fr: fr,
        disabledDates: {
          to: new Date(),
          dates: gon.dates_non_disponibles.map(function(date) {
            return new Date(date)
          })
        }
      }
    },
    components: { Datepicker }
  })
})
