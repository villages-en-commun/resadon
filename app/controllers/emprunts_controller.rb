class EmpruntsController < ApplicationController
  before_action :require_login, only: :index

  def new
    @empruntable = Empruntable.find(params[:empruntable_id])
    @emprunt = Emprunt.new
    gon.dates_non_disponibles = @empruntable.non_disponible
  end

  def create
    @empruntable = Empruntable.find(params[:empruntable_id])
    @emprunt = Emprunt.new(emprunt_params)
    @emprunt.empruntable = @empruntable
    if @emprunt.save
      UserMailer.with(emprunt: @emprunt).confirmation.deliver_later
      redirect_to new_empruntable_emprunt_path(@empruntable), notice: "Nous venons de vous envoyer un mail de confirmation"
    else
      gon.dates_non_disponibles = @empruntable.non_disponible
      render :new
    end
  end

  def index
    @emprunts = Emprunt.all
  end

  private

  def emprunt_params
    params.require(:emprunt).permit(:nom, :prenom, :tel, :email, :date_emprunt, :date_restitution)
  end
end
