class EmpruntablesController < ApplicationController
  before_action :set_empruntable, only: [:show, :edit, :update, :destroy]

  def index
    @empruntables = Empruntable.all
  end
end
