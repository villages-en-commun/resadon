json.extract! empruntable, :id, :nom, :description_courte, :description_longue, :created_at, :updated_at
json.url empruntable_url(empruntable, format: :json)
