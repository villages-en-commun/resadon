Clearance.configure do |config|
  config.allow_sign_up = false
  config.mailer_sender = ENV['FROM_EMAIL']
  config.rotate_csrf_on_sign_in = true
  config.routes = false
  config.redirect_url = '/emprunts'
end
