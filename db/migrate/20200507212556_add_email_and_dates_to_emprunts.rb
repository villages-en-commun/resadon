class AddEmailAndDatesToEmprunts < ActiveRecord::Migration[6.0]
  def change
    add_column :emprunts, :email, :string
    add_column :emprunts, :date_emprunt, :date
    add_column :emprunts, :date_restitution, :date
  end
end
