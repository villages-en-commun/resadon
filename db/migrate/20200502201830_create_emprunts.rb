class CreateEmprunts < ActiveRecord::Migration[6.0]
  def change
    create_table :emprunts do |t|
      t.references :empruntable, null: false, foreign_key: true
      t.string :nom
      t.string :prenom
      t.string :tel

      t.timestamps
    end
  end
end
