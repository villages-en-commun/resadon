class CreateEmpruntables < ActiveRecord::Migration[6.0]
  def change
    create_table :empruntables do |t|
      t.string :nom
      t.string :description_courte
      t.string :description_longue

      t.timestamps
    end
  end
end
